
class ArrayList {
    constructor(array, ...rest) {
        this.array = array;
        this.rest = rest;
    }

    push() {
        if (this.validator()) {
            return this.validator()
        }
        for (let i = 0; i < this.rest.length; i++) {
            this.array[this.array.length] = this.rest[i];
        }
        return true;
    }

    pop() {
        if (this.validator(1)) {
            return this.validator(1)
        }
        let delItem = this.array[this.array.length - 1];
        this.array.length = this.array.length - 1;
        return delItem;
    }

    shift() {
        if (this.validator(1)) {
            return this.validator(1)
        }
        let delItem = this.array[0];
        for (let i = 1; i < this.array.length; i++) {
            this.array[i - 1] = this.array[i];
        }
        this.pop();

        return delItem;

    }

    unshift() {
        if (this.validator()) {
            return this.validator()
        }
        for (let i = this.array.length - 1; i >= 0; i--) {
            this.array[i + this.rest.length] = this.array[i];
            if (i < this.rest.length) {
                this.array[i] = this.rest[i];
            }
        }

        return true;

    }

    toString() {
        if (this.validator()) {
            return this.validator()
        }
        let string = '', arrLen = this.array.length - 1;

        this.array.forEach(function (item, i) {
            if (i === arrLen) {
                string += item
            } else {
                string += item + ',';
            }

        });
        return string;
    }

    veryQuickSort() {
        if (this.validator()) {
            return this.validator()
        }
        const length = this.array.length;

        for (let i = length; i >= 0; i--) {

            for (let x = 0; x <= i - 1; x++) {

                if (this.array[x] > this.array[x + 1]) {

                    let tmp = this.array[x];

                    this.array[x] = this.array[x + 1];

                    this.array[x + 1] = tmp;

                }
            }
        }

        return this.array
    }

    validator(securityKey = 0) {
        const err = "Не коректные данные. Нужно передать массив с данными";

        if (!Array.isArray(this.array)) {
            return err
        } else if (securityKey === 1 && this.array.length === 0  ) {
            return err
        }
        return false;
    }


}
let arr = [123,56,-7,8,99,-2,5];
let lArray = new ArrayList(arr, 111111, 444, -67, 89);

