describe('ArrayList', function () {
    describe('push', function () {
        it('function check', function () {
            const arrN = [123,56,-7,8,99,-2,5];
            const rest = [111111];

            const actual = new ArrayList(arrN,rest).push();
            const expected = true;

            assert.strictEqual(actual, expected);
        });
        it('function check rest', function () {
            const arrN = [123,56,-7,8,99,-2,5];
            const rest = [111111, 444, -67, 89];

            const actual = new ArrayList(arrN,rest).push();
            const expected = true;

            assert.strictEqual(actual, expected);
        });
        it('function check arrN.length++', function () {
            const arrN = [123,56,-7,8,99,-2,5];
            const rest = [111111];

            const expected = arrN.length + 1;
            const arrPop= new ArrayList(arrN,rest).push();
            const actual = arrN.length;

            assert.strictEqual(actual, expected);
        });

        it('data validate no correct', function () {
            const actual = new ArrayList('undefined').push();
            const expected = "Не коректные данные. Нужно передать массив с данными";

            assert.strictEqual(actual, expected);
        });
    });
    describe('pop', function () {
        it('function check', function () {
            const arrN = [123,56,-7,8,99,-2,5];

            const expected = arrN[arrN.length - 1];
            const actual = new ArrayList(arrN).pop();

            assert.strictEqual(actual, expected);
        });
        it('function check arrN.length--', function () {
            const arrN = [123,56,-7,8,99,-2,5];


            const expected = arrN.length - 1;
            const arrPop= new ArrayList(arrN).pop();
            const actual = arrN.length;

            assert.strictEqual(actual, expected);
        });

        it('data validate no correct', function () {
            const actual = new ArrayList('undefined').pop();
            const expected = "Не коректные данные. Нужно передать массив с данными";

            assert.strictEqual(actual, expected);
        });
    });
    describe('shift', function () {
        it('function check', function () {
            const arrN = [123,56,-7,8,99,-2,5];

            const expected = arrN[0];
            const actual = new ArrayList(arrN).shift();

            assert.strictEqual(actual, expected);
        });
        it('function check arrN.length++', function () {
            const arrN = [123,56,-7,8,99,-2,5];

            const expected = arrN.length - 1;
            const arrPop= new ArrayList(arrN).shift();
            const actual = arrN.length;

            assert.strictEqual(actual, expected);
        });

        it('data validate no correct', function () {
            const actual = new ArrayList(null).shift();
            const expected = "Не коректные данные. Нужно передать массив с данными";

            assert.strictEqual(actual, expected);
        });
    });
    describe('unshift', function () {
        it('function check', function () {
            const arrN = [123,56,-7,8,99,-2,5];
            const rest = [111111, 444, -67, 89];

            const actual = new ArrayList(arrN,rest).unshift();
            const expected = true;

            assert.strictEqual(actual, expected);
        });
        it('function check rest', function () {
            const arrN = [123,56,-7,8,99,-2,5];
            const rest = [111111, 444, -67, 89];

            const actual = new ArrayList(arrN,rest).unshift();
            const expected = true;

            assert.strictEqual(actual, expected);
        });
        it('function check arrN.length++', function () {
            const arrN = [123,56,-7,8,99,-2,5];
            const rest = [111111];

            const expected = arrN.length + 1;
            const arrPop= new ArrayList(arrN,rest).unshift();
            const actual = arrN.length;

            assert.strictEqual(actual, expected);
        });

        it('data validate no correct', function () {
            const actual = new ArrayList(NaN).unshift();
            const expected = "Не коректные данные. Нужно передать массив с данными";

            assert.strictEqual(actual, expected);
        });
    });
    describe('toString', function () {
        it('function check', function () {
            const arrN = [123,56,-7,8,99,-2,5];

            const actual = new ArrayList(arrN).toString();
            const expected = "123,56,-7,8,99,-2,5";

            assert.strictEqual(actual, expected);
        });
        it('data validate no correct', function () {
            const actual = new ArrayList(123).toString();
            const expected = "Не коректные данные. Нужно передать массив с данными";

            assert.strictEqual(actual, expected);
        });
    });
    describe('veryQuickSort', function () {
        it('function check', function () {
            const arrN = [123,56,-7,8,99,-2,5];

            const actual = new ArrayList(arrN).veryQuickSort();
            const expected = [-7,-2,5,8,56,99,123];

            assert.deepEqual(actual, expected);
        });
        it('data validate no correct', function () {
            const actual = new ArrayList(123).veryQuickSort();
            const expected = "Не коректные данные. Нужно передать массив с данными";

            assert.strictEqual(actual, expected);
        });
    });
});
